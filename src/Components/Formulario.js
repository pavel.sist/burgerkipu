import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
}));

export default function Formulario(com) {

    const classes = useStyles();

    const consultarApi = async () => {
        console.log(com)
    }


    return (
        <Fragment>
            <h2> ORDEN PEDIDO</h2>
            <form className={classes.root} noValidate autoComplete="off">
                <FormControl><Input defaultValue="Disabled" disabled inputProps={{ 'aria-label': 'description' }} /></FormControl>
                <FormControl><Input defaultValue="Disabled" disabled inputProps={{ 'aria-label': 'description' }} /></FormControl>


            </form>

            <div style={{ alignContent: 'center', alignItems: 'center' }}>

                <Button style={{ margin: 5, height: 55, width: 500, background: "#2ea44e", color: "white", fontWeight: "bold", fontSize: 24 }} onClick={consultarApi} variant="contained">ENVIAR</Button>


            </div>

            <div style={{ alignContent: 'center', alignItems: 'center' }}>

                <Button style={{ margin: 5, height: 55, width: 500, background: "white", color: "#bc6b71", fontWeight: "bold", fontSize: 24 }} onClick={consultarApi} variant="contained">CANCELAR</Button>


            </div>


        </Fragment>
    )
}
