import React, { Fragment, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import ComidaItem from './ComidaItem'

import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import IconButton from '@material-ui/core/IconButton';

// const listComidas = [
//     {
//         id: 'a',
//         denominacion: 'Hamburgues Doble',
//         detalle: 'comida',
//         precio: 12,
//     },
//     {
//         id: 'b',
//         denominacion: 'Hamburgues Triple',
//         detalle: 'comida',
//         precio: 15,
//     },

// ];
const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {

        height: 500,
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
}));

export default function Comidas(com) {
    const classes = useStyles();
    const [listComidas, setlistComidas] = useState([]);

    const consultarApi = async () => {
        const api = await fetch('https://rickandmortyapi.com/api/character/');
        const listComida = await api.json();
        console.log(listComida.results);
        setlistComidas(listComida.results);
    }
    const agregarCarrito = (comida) => {
        console.log(comida);
        com = comida;
    }


    return (
        <Fragment>

            <div className="row">
                <Button style={{ flex: 1, margin: 5, height: 55, background: "#b85d26", color: "white", fontWeight: "bold", fontSize: 24 }} onClick={consultarApi} variant="contained">DESAYUNO</Button>
                <Button style={{ flex: 1, margin: 5, height: 55, background: "#81643a", color: "white", fontWeight: "bold", fontSize: 24 }} onClick={consultarApi} variant="contained">ALMUERZO</Button>
                <Button style={{ flex: 1, margin: 5, height: 55, background: "#b75d28", color: "white", fontWeight: "bold", fontSize: 24 }} onClick={consultarApi} variant="contained">BEBIDAS</Button>

            </div>


            <GridList cols={3} cellHeight={180} className={classes.gridList}>

                {listComidas.map((comida) => (
                    <GridListTile key={comida.id}>
                        <img src={comida.image} alt={comida.title} onClick={() => agregarCarrito(comida)} />
                        <GridListTileBar
                            title={comida.name}
                            subtitle={<span>by: {comida.status}</span>}
                            actionIcon={
                                <IconButton aria-label={`info about ${comida.title}`} className={classes.icon}>

                                </IconButton>
                            }
                        />
                    </GridListTile>
                ))}
            </GridList>

        </Fragment>
    )
}
