import React, { Fragment, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import Formulario from './Components/Formulario';
import Comidas from './Components/Comidas';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));


function App() {
  const [com, setcom] = useState({});
  const classes = useStyles();
  return (
    <Fragment>
      <h1>
        Kipu Burger
    </h1>
      <Grid container spacing={3}>

        <Grid item xs={6}>
          <Paper className={classes.paper}>
            <Formulario com={com}></Formulario>
          </Paper>
        </Grid>
        <Grid item xs={6}>
          <Paper className={classes.paper}>
            <Comidas com={com}></Comidas>
          </Paper>
        </Grid>

      </Grid>

    </Fragment>

  );
}

export default App;
